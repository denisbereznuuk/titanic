import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    if not isinstance(df, pd.DataFrame):
        return None
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    result = [('Mr.', 119, 30), ]
    for title in ['Mrs.', 'Miss.']:
        result.append((title, int(df[(df['Name'].str.contains(title)) & (df['Age'].isnull())].shape[0]),
               int(df[df['Name'].str.contains(title)]['Age'].median())))
    return result
